var Transducer = require('./transducer');

function transformArticles(reditData, action){
  var articles = reditData.data.children;
  var transducer = new Transducer(articles);

  switch(action.type){
    case 'sort':
      var direction = action.params && action.params.dir;
      return transducer.pluck('data').select(['id', 'score', 'created_utc', 'title']).sort('score', direction).extract();

    case 'aggregate':
      return transducer.pluck('data').select(['domain', 'score']).aggregate('domain').extract();

    default:
      return transducer.pluck('data').extract();
  }
}

module.exports.transformArticles = transformArticles;