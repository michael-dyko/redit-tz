'use strict';
var _ = require('underscore');
function ForestFactory() {
}

ForestFactory.prototype.grow = function(plainData) {
  if(!_.isArray(plainData)){
    return [];
  }

  //filter incorrect data
  plainData = _.filter(plainData, function(element){
    return (_.isNumber(element.id) && _.isNumber(element.parentId));
  })
  // We need sorted list to make our algorithm work
  plainData = _.sortBy(plainData, 'id');

  var forest = [];
  var nodes = {};
  _.each(plainData, function(node){
    //creating new node and adding to nodes list
    var newNode = {
      id: node.id,
      parentId: node.parentId
    };
    nodes[node.id] = newNode;

    //take parent node from nodes
    var parentNode = nodes[node.parentId]
    //if parent node is undefined than this node is new root
    if(!parentNode) {
      forest.push(newNode);
    } else {
      if(!parentNode.children){
        parentNode.children = [];
      }
      parentNode.children.push(newNode);
    }
  })
  return forest;
};

module.exports = ForestFactory;
