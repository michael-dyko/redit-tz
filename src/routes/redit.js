'use strict';
var express = require('express');
var router = express.Router();
var request = require('request')
var JSONStream = require('JSONStream');
var es = require('event-stream');

var articleController = require('../articleController.js');

router.get('/redit', function(req, res){
  request({url: req.query.link})
    .pipe(JSONStream.parse())
    .pipe(es.mapSync(function (data) {
      var actionParameters = {};
      try {
        actionParameters = JSON.parse(req.query.actParams);
      } catch(e){
        console.error(e);
      }
      var action = {type: req.query.action, params: actionParameters};

      return articleController.transformArticles(data, action);
    }))
    .pipe(es.mapSync(function (data) {
      var template = getTemplate(req.query.template);
      var templateParameters = {};
      try {
        templateParameters = JSON.parse(req.query.tempParams);
      } catch(e){
        console.error(e);
      }
      res.render(template,  {data: data, params:templateParameters});
    }))
});

function getTemplate(type){
  switch (type){
    case 'csv':
      return 'csvTemplate.pug';
    case 'sql':
      return 'sqlTemplate.pug';
    default:
      return 'csvTemplate.pug';
  }
}
module.exports = router;

