'use strict';
var _ = require('underscore');

/*
/*
/* Transducer is wrapper for underscore
/* We can add new function
/* We can change underscore with anything else
 */

function Transducer(data) {
  this._data = data;
}

Transducer.prototype.extract = function() {
  return this._data;
};

Transducer.prototype.dataType = function() {
  if(this._data instanceof Array){
    return 'Array'
  }  else if(this._data instanceof Object){
    return 'Object'
  }
  return 'NotObject'
};

Transducer.prototype.sort = function(key, direction) {
  if(!key){
    return this
  }
  //we can sort only array data
  if(this.dataType() === 'Array'){
    this._data = _.sortBy(this._data, key)
    if(direction === 'desc'){
      this._data = this._data.reverse()
    }
  }
  return this;
};

Transducer.prototype.select = function(keys){
  if(!keys){
    return this
  }
  //we can select only in array data
  if(this.dataType() === 'Array'){
    this._data = _.map(this._data, function(element){
      return _.pick.apply(null, [element].concat(keys))
    })
  }
  return this;
}

Transducer.prototype.pluck = function(key){
  if(!key){
    return this
  }
  //we can select only in array data
  if(this.dataType() === 'Array'){
    this._data = _.map(this._data, function(element){
      return element[key];
    })
  }
  return this;
}

Transducer.prototype.aggregate = function(key){
  if(!key){
    return this;
  }
  //we can aggregate only array data
  if(this.dataType() === 'Array'){
    var groups = _.groupBy(this._data, key);

    this._data = _.map(groups, function(group, groupKey) {
      var newElement = _.reduce(group, function(memo, element){
        if(!memo){
          return element;
        } else {
          return objectConcat(memo, element);
        }
      }, null);

      newElement[key] = groupKey;
      newElement.count = group.length;
      return newElement;
    });
  }
  return this;
}

function objectConcat(first, second){
  var result = {};
  _.each(first, function(value, key){
    var secondValue = second[key]

    if(typeof value === 'number' && typeof secondValue === 'number'){
      result[key] = value + secondValue;
    } else{
      result[key] = value;
    }
  });

  return result;
}

module.exports = exports =  Transducer;