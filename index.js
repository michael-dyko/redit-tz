var express = require('express');
var app = express();

var redit = require('./src/routes/redit.js');

var api = express.Router();
api.use('', redit);
app.use(api);
app.set('view engine', 'pug');
app.set('views', './src/views')
app.listen(5000);

module.exports = app;