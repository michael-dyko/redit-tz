var chai = require('chai');
var expect = chai.expect;
var chaiHttp = require('chai-http');
var server = require('../index.js');
chai.use(chaiHttp);

var testLink = 'https://jsonblob.com/api/jsonBlob/5763a934-9136-11e7-b0e4-1d826cc771e6'
describe('/redit', function() {
  it('should return sorted articles in CSV', function(done) {
    chai.request(server)
      .get('/redit')
      .query({link: testLink, action: 'sort', actParams:'{"dir":"desc"}', template:'csv', tempParams:'{"div":","}'})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(res.text).to.equal(sortCSVData)
        done();
      });
  });

  it('should return aggregated articles in CSV', function(done) {
    chai.request(server)
      .get('/redit')
      .query({link: testLink, action: 'aggregate', actParams:'{}', template:'csv', tempParams:'{}'})
      .end(function(err, res) {
        expect(res).to.have.status(200);
        expect(res.text).to.equal(aggregatedCSVData)
        done();
      });
  });

});

var sortCSVData = '6waspe,189,1503815734,Do we need a JavaScriptHelp subreddit?</br>6xl8sm,176,1504350253,Javascript : The Curious Case of Null &amp;gt;= 0</br>6xj93p,118,1504319252,All Hands On Deck – How you can use your skills to contribute to Firefox 57 success</br>6xsk37,69,1504436564,Image Processing Component For React</br>6xtu6f,60,1504453433,Let’s talk about Javascript string encoding</br>6xlmb7,38,1504355399,Here\'s a particles.js implementation that uses custom moving images instead and is fully customizable</br>6xtitr,22,1504450023,Snap shot plus data-driven testing for Mocha and other BDD test runners</br>6xrep6,18,1504415556,Build a Photo Filter Application</br>6xv3qm,13,1504465843,Today I learned you can label if statements to break out of nested if\'s</br>6xpy6z,11,1504397293,Is there an easier way to curry functions that this?</br>6xmudp,9,1504368015,The Grave Accent and XSS</br>6xs65g,8,1504429368,Do I need to use semi colons?</br>6xpoor,7,1504394062,Best way to store obj.methods in localStorage? Or another way to go around this issue?</br>6xrl3i,5,1504418253,Any Javascript learning resources that assume that you have experience in another programming language in the past?</br>6xs31h,5,1504427721,A jest transformer for .vue files - with source maps 👌</br>6xu95x,5,1504457555,Where do leap seconds go?</br>6xvgxq,5,1504469540,How to call C/C++ code from Node.js</br>6xnqf7,3,1504375578,Interesting SO Question: Permissions to created directories using Yarn</br>6xtvv0,3,1504453924,Help With Node Project</br>6xurzd,3,1504462518,Rekapi, the JavaScript Keyframe Library, reaches 2.0.0</br>6xyer5,3,1504502921,Fast Properties in V8</br>6xoqng,2,1504384036,Release textics-stream (Text Statistics For Streams)</br>6xnaz3,2,1504371824,How would you make a stay logged in system using react, express and mongodb</br>6xlfdh,2,1504353027,Showoff Saturday (September 02, 2017)</br>6xu5p0,1,1504456613,Side project : SnapCheap, websockets picture chat</br>6xsj4h,0,1504436025,Loading menu items on the same page below the menu?</br>6xu90p,0,1504457512,Not able to hit the route successfully to register user for my MEAN app. Need help</br>';
var aggregatedCSVData = 'self.javascript,245,11</br>kev.inburke.com,60,1</br>v8project.blogspot.co.uk,3,1</br>github.com,136,5</br>medium.com,23,2</br>jeremyckahn.github.io,3,1</br>hello.snap.cheap,1,1</br>blog.campvanilla.com,176,1</br>stackoverflow.com,3,2</br>diary.braniecki.net,118,1</br>davidmurdoch.com,9,1</br>'