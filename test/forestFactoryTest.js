var chai = require('chai');
var expect = chai.expect;

var ForestFactory = require('../src/forestFactory.js');

describe('ForestFactory', function() {
  it('should have grow function', function() {
    var forestFactory = new ForestFactory()
    expect(forestFactory).to.have.property('grow').that.is.a('function');

    describe('have.grow', function() {
      it('return empty array for empty array', function(){
        var data = []
        var forestFactory = new ForestFactory()
        var forest = forestFactory.grow(data)
        expect(forest).to.eql([])
      })

      it('return root for one element', function(){
        var data = [{id: 1, parentId: 0}]
        var forestFactory = new ForestFactory()
        var forest = forestFactory.grow(data)
        expect(forest).to.eql([{id: 1, parentId: 0}])
      })

      it('return several roots', function(){
        var data = [{id: 1, parentId: 0},{id: 3, parentId: 2},{id: 5, parentId: 4}]
        var forestFactory = new ForestFactory()
        var forest = forestFactory.grow(data)
        expect(forest).to.eql([{id: 1, parentId: 0},{id: 3, parentId: 2},{id: 5, parentId: 4}])
      })

      it('filter incorrect data (without id or parentId)', function(){
        var data = [{id: 1, parentId: 0},{id: 2, parentId: 1},{id: 3, parentId: 1},{id: 3}]
        var forestFactory = new ForestFactory()
        var forest = forestFactory.grow(data)
        expect(forest).to.eql([{id: 1, parentId: 0, children:[{id: 2, parentId: 1},{id: 3, parentId: 1}]}])
      })

      it('grow forest from TZ', function(){
        var data = [{id: 1, parentId: 0},
            {id: 2, parentId: 0},
            {id: 3, parentId: 1},
            {id: 4, parentId: 1},
            {id: 5, parentId: 2},
            {id: 6, parentId: 4},
            {id: 7, parentId: 5}
          ];
        var forestFactory = new ForestFactory()
        var forest = forestFactory.grow(data)
        expect(forest).to.eql(expectedForest)
      })

      it('grow forest for unsorted data', function(){
        var data = [
          {id: 3, parentId: 1},
          {id: 1, parentId: 0},
          {id: 5, parentId: 2},
          {id: 2, parentId: 0},
          {id: 6, parentId: 4},
          {id: 4, parentId: 1},
          {id: 7, parentId: 5}
        ];
        var forestFactory = new ForestFactory()
        var forest = forestFactory.grow(data)
        expect(forest).to.eql(expectedForest)
      })

    })
  })
})

var expectedForest = [
  {
    "id": 1,
    "parentId": 0,
    "children": [
      {
        "id": 3,
        "parentId": 1
      },
      {
        "id": 4,
        "parentId": 1,
        "children": [
          {
            "id": 6,
            "parentId": 4
          }
        ]
      }
    ]
  },
  {
    "id": 2,
    "parentId": 0,
    "children": [
      {
        "id": 5,
        "parentId": 2,
        "children": [
          {
            "id": 7,
            "parentId": 5
          }
        ]
      }
    ]
  }
]