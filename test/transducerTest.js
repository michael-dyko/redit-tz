var chai = require('chai');
var expect = chai.expect;
var Transducer = require('../src/transducer.js');
describe('Transducer', function() {
  it('should have extract function', function() {
    var transducer = new Transducer(mockData)
    expect(transducer).to.have.property('extract').that.is.a('function');

    describe('transducer.extract', function() {
      it('should return current data', function(){
        var transducer = new Transducer({a:1, b:'zxc'})
        expect(transducer.extract()).to.eql({a:1, b:'zxc'})
      })
    })
  })

  it('should have sort function', function() {
    var transducer = new Transducer(mockData)
    expect(transducer).to.have.property('sort').that.is.a('function');

    describe('transducer.sort', function() {
      it('return the transducer object for chaining', function(){
        var data = [{a:3}, {a:1}, {a:2}]
        var transducer = new Transducer(data)
        expect(transducer.sort()).to.eql(transducer)
      })

      it('sort by field in if current data is array of object', function(){
        var data = [{a:3}, {a:1}, {a:2}]
        var transducer = new Transducer(data)
        var ascSortData = transducer.sort('a', 'asc').extract()
        expect(ascSortData).to.eql([{a:1}, {a:2}, {a:3}])
      })
      it('sort in desc direction', function(){
        var data = [{a:3}, {a:1}, {a:2}]
        var transducer = new Transducer(data)
        var ascSortData = transducer.sort('a', 'desc').extract()
        expect(ascSortData).to.eql([{a:3}, {a:2}, {a:1}])
      })
      it('choose asc as default direction', function(){
        var data = [{a:3}, {a:1}, {a:2}]
        var transducer = new Transducer(data)
        var ascSortData = transducer.sort('a').extract()
        expect(ascSortData).to.eql([{a:1}, {a:2}, {a:3}])
      })
    })
  })

  it('should have select function', function() {
    var transducer = new Transducer(mockData)
    expect(transducer).to.have.property('select').that.is.a('function');

    describe('transducer.select', function () {
      it('return the transducer object for chaining', function () {
        var data = [{a: 3, b:'abc'}, {a: 1, b:'abc'}, {a: 2, b:'abc'}]
        var transducer = new Transducer(data)
        expect(transducer.select()).to.eql(transducer)
      })

      it('return objects with selected key', function () {
        var data = [{a: {c:3}, b:'abc'}, {a: {c:4}, b:'abc'}, {a: {c:5}, b:'abc'}]
        var keys = ['a']
        var transducer = new Transducer(data)
        var selectedData =  transducer.select(keys).extract()
        expect(selectedData).to.eql([{a:{c:3}}, {a:{c:4}}, {a:{c:5}}])
      })

      it('return objects with selected keys', function () {
        var data = [{a: 3, b:'abc', c:true}, {a: 1, b:'abc', c:true}, {a: 2, b:'abc', c:true}]
        var keys = ['a', 'b']
        var transducer = new Transducer(data)
        var selectedData =  transducer.select(keys).extract()
        expect(selectedData).to.eql([{a:3, b:'abc'}, {a:1, b:'abc'}, {a:2, b:'abc'}])
      })
    })
  })

  it('should have aggregate function', function() {
    var transducer = new Transducer(mockData)
    expect(transducer).to.have.property('aggregate').that.is.a('function');

    describe('transducer.aggregate', function () {
      it('return the transducer object for chaining', function () {
        var data = [{a: 3, b:'abc'}, {a: 1, b:'abc'}, {a: 2, b:'abc'}]
        var transducer = new Transducer(data)
        expect(transducer.aggregate()).to.eql(transducer)
      })

      it('group elements with same key', function () {
        var data = [{id: 'a'}, {id: 'b'}, {id: 'a'}]
        var transducer = new Transducer(data)
        expect(transducer.aggregate('id').extract()).to.eql([{id:'a', count: 2},{id:'b', count: 1}])
      })

      it('should sum Number field', function () {
        var data = [{id: 'a', num: 3}, {id: 'b', num: 4}, {id: 'a', num: 5}]
        var transducer = new Transducer(data)
        expect(transducer.aggregate('id').extract()).to.eql([{id:'a', num: 8, count: 2},{id:'b', num: 4, count: 1}])
      })
    })
  })

  describe('chaining', function(){
    it('select and sort', function () {
      var transducer = new Transducer(mockData)
      var newData = transducer.pluck('data').select(['id', 'score', 'created_utc', 'title']).sort('score', 'desc').extract()
      expect(newData).to.eql(selectedAndSorted)
    })

    it('select and aggregate and sort', function () {
      var transducer = new Transducer(mockData)
      var newData = transducer.pluck('data').select(['domain', 'score']).aggregate('domain').extract()
      expect(newData).to.eql(aggregatedData)
    })

  })

})

var selectedAndSorted = [
  {
    "created_utc": 1380140652,
    "id": "1n4m6p",
    "score": 16,
    "title": "Functional Mixins and Advice in JavaScript"
  },
  {
    "created_utc": 1380129576,
    "id": "1n46by",
    "score": 14,
    "title": "Template inheritance for Angular JS"
  }]

var aggregatedData = [
  {
    "domain": 'rathercurio.us',
    "score": 16,
    "count": 1
  },
  {
    "domain": 'bunselmeyer.net',
    "score": 14,
    "count": 1
  }]

var mockData = [{
  "kind": "t3",
  "data": {
    "domain": "rathercurio.us",
    "banned_by": null,
    "media_embed": {},
    "subreddit": "javascript",
    "selftext_html": null,
    "selftext": "",
    "likes": null,
    "secure_media": null,
    "saved": false,
    "id": "1n4m6p",
    "secure_media_embed": {},
    "clicked": false,
    "stickied": false,
    "author": "masterJ",
    "media": null,
    "score": 16,
    "approved_by": null,
    "over_18": false,
    "hidden": false,
    "thumbnail": "",
    "subreddit_id": "t5_2qh30",
    "edited": false,
    "link_flair_css_class": null,
    "author_flair_css_class": null,
    "downs": 2,
    "is_self": false,
    "permalink": "/r/javascript/comments/1n4m6p/functional_mixins_and_advice_in_javascript/",
    "name": "t3_1n4m6p",
    "created": 1380169452.0,
    "url": "http://rathercurio.us/functional-mixins-and-advice-in-javascript",
    "author_flair_text": null,
    "title": "Functional Mixins and Advice in JavaScript",
    "created_utc": 1380140652.0,
    "link_flair_text": null,
    "ups": 18,
    "num_comments": 8,
    "num_reports": null,
    "distinguished": null
  }
}, {
  "kind": "t3",
  "data": {
    "domain": "bunselmeyer.net",
    "banned_by": null,
    "media_embed": {},
    "subreddit": "javascript",
    "selftext_html": null,
    "selftext": "",
    "likes": null,
    "secure_media": null,
    "saved": false,
    "id": "1n46by",
    "secure_media_embed": {},
    "clicked": false,
    "stickied": false,
    "author": "tpk1024",
    "media": null,
    "score": 14,
    "approved_by": null,
    "over_18": false,
    "hidden": false,
    "thumbnail": "",
    "subreddit_id": "t5_2qh30",
    "edited": false,
    "link_flair_css_class": null,
    "author_flair_css_class": null,
    "downs": 0,
    "is_self": false,
    "permalink": "/r/javascript/comments/1n46by/template_inheritance_for_angular_js/",
    "name": "t3_1n46by",
    "created": 1380158376.0,
    "url": "http://www.bunselmeyer.net/#!/thoughts/angular-blocks",
    "author_flair_text": null,
    "title": "Template inheritance for Angular JS",
    "created_utc": 1380129576.0,
    "link_flair_text": null,
    "ups": 14,
    "num_comments": 0,
    "num_reports": null,
    "distinguished": null
  }
}]